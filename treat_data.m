data = csvread('data.csv');

nMachines = data(:,1);
nPetitions = data(:,2);
improvement = data(:,4);

M = zeros(10,10);

for m = 1:10
  for p = 1:10
    v = improvement(nMachines==m & nPetitions==p);
    if length(v) > 0
      M(m,p) = 100*mean(v);
    end
  end
end

my_bar3(M);

xlabel('nPetitions')
ylabel('nMachines')
zlabel('mean improvement (%)')
title('Comparison of number of steps of basic version vs heuristic version')

Y = zeros(10,1);

for p = 1:10
  v = improvement(nPetitions==p);
  if length(v) > 0
    Y(p) = 100*mean(v);
  end
end

figure;
bar(Y);
xlabel('nPetitions')
ylabel('mean improvement (%)')
title('Comparison of number steps of basic version vs heuristic version')


Y = zeros(10,1);

for m = 1:10
  v = improvement(nMachines==m);
  if length(v) > 0
    Y(m) = 100*mean(v);
  end
end


figure;
bar(Y);
xlabel('nMachines')
ylabel('mean improvement (%)')
title('Comparison of number steps of basic version vs heuristic version')
