#Specific purpose planner#

Specific purpose linear planner. It aims to solve a particular problem using a STRIPS-like
algorithm.

The problem roughly consists in delivering coffees in a workplace with many offices. Each
worker demands a certain amount of coffee cups and a robot has to take care of getting
the coffee from the machines and bringing it to the workers. The workplace is represented
as a grid, and the planner tries to minimise the time during which the robot is
wandering around (i.e. it has to come up with a plan for a fast and correct delivery).

Members of the team:

    * Salvador Medina
    * Alejandro Suárez
    
## What can be found here

  + The source code of the project (src) folder. The project can be imported from
  Eclipse. In order to do so one has to go to File>Import... Then select Existing project into
  workspace. Then choose Select root directory and set as the root directoy this one (the same
  where the README.md is located). Perhaps it is necessary to tell Eclipse the correct JRE.
  To do so, go to the project's properties, and then go to the Java Build Path>Libraries
  tab. Edit the JRE System Library to match that of your System (it should work with both
  Java 8 and Java 7). The project can be imported from IntelliJ IDE as well.

  + An already compiled and packaged .jar file. It is ready for execution, without external
  dependencies. It is located at the dist/ subfolder. To execute it with one of the benchmark
  examples you must be in the dist/ directory. Then run:
  ```
  java -jar CoffeeServerPlanner.jar ../benchmark/instance_9_9_1.in -he ClosestToRobot -o out.txt
  ```
  This will solve the problem instance_9_9_1.in from the benchmark folder with
  heuristic ClosestToRobot, adding the implicit
  predicates Step(0) and Robot-free. It is possible to indicate that all the predicates are explicit:
  ```
  java -jar CoffeeServerPlanner.jar ../benchmark/contrived/contrived_1.in -he ClosestToRobot -ni -o out.txt
  ```
  To get the full list of options run the application without parameters.
  ```
  java -jar CoffeeServerPlanner.jar
  ```
  
  + A folder called benchmark with several instances of randomly generated problems. The
  convention for the names is instance_x_y_z.in where x is the number of machines, y is
  the number of petitions and z is the number of the instance (several instances for each
  x,y). There is also a contrived subfolder inside benchmark that contains some hand-written
  contrived examples.
  
  + A folder called uml with the UML diagrams
  
  + A folder called plots with some graphical results
  
  + The report of the project (report folder).
  
  + Some auxiliary Octave scripts (my_bar3.m and treat_data.m) that have been used to
  create the plots inside the plots folder.
  
  + A doc folder that contains the JavaDocs for this project.
