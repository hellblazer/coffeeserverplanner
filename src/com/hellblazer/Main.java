package com.hellblazer;

import com.hellblazer.heuristics.ClosestToRobotHeuristic;
import com.hellblazer.heuristics.Heuristic;
import com.hellblazer.misc.CoffeeServerPlannerException;
import com.hellblazer.operations.Operation;
import com.hellblazer.stack.PlannerStack;
import com.hellblazer.state.Problem;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * Main class. Contains the main method. Check printUsage for more info
 * about how to use this program.
 */
public class Main {

    private static void printUsage(){
        System.out.println("CoffeeServerPlanner usage:");
        System.out.println("./CoffeeServerPlanner INPUT_FILE [OPTIONS]");
        System.out.println();
        System.out.println("INPUT_FILE is a file with the practice's definition specified format:");
        System.out.println("InitialState=PREDICATE_I1;PREDICATE_I2;...;PREDICATE_IN");
        System.out.println("GoalState=PREDICATE_I1;PREDICATE_I2;...;PREDICATE_IK");
        System.out.println();
        System.out.println("Supported PREDICATEs are:");
        System.out.println("Robot-location(OFFICE)");
        System.out.println("Machine(OFFICE,CAPACITY)");
        System.out.println("Petition(OFFICE,AMOUNT)");
        System.out.println("Robot-free()");
        System.out.println("Robot-loaded(AMOUNT)");
        System.out.println("Served(OFFICE)");
        System.out.println("Steps(AMOUNT)");
        System.out.println("");
        System.out.println("Supported OPTIONS are:");
        System.out.println("-o (--output) FILENAME : Writes the different states of the stack through the execution");
        System.out.println("-h (--help) : Prints help");
        System.out.println("-he (--heuristic) HEURISTIC : Sets the specified heuristic");
        System.out.println("-ni (--no-implicit) : Prevents the system from adding the implicit predicates: RobotFree() and Steps(0)");
        System.out.println("");
        System.out.println("Supported HEURISTICs are:");
        System.out.println("None : Sets to the null heuristic. Nothing additional is done.");
        System.out.println("ClosestToRobot : Tries to always serve requests that are the closest to the robot.");


    }

    public static void main(String[] args) {

        if (args.length == 0) {
            printUsage();
        } else {
            String filename = args[0];
            PrintWriter output = null;
            Heuristic heuristic = null;
            boolean addImplicit = true;

            for (int i = 1; i < args.length; i++) {
                switch (args[i]) {
                    case "-h":
                    case "--help":
                        printUsage();
                        return;
                    case "-ni":
                    case "--no-implicit":
                        addImplicit=false;
                        break;
                    case "-o":
                    case "--output":
                        try {
                            output = new PrintWriter(args[++i]);
                        } catch (FileNotFoundException e) {
                            System.out.println("ERROR: Invalid file: "+args[i]);
                        }
                        break;
                    case "-he":
                    case "--heuristic":
                        switch(args[++i]) {
                            case "None":
                                heuristic = new Heuristic();
                                break;
                            case "ClosestToRobot":
                                heuristic = new ClosestToRobotHeuristic();
                                break;
                            default:
                                System.out.println("ERROR: Invalid Heuristic: "+args[i]);
                                return;
                        }
                        break;
                    default:
                        System.out.println("ERROR: Invalid Option: "+args[i]);
                        return;
                }
            }

            // Begin the execution
            StringBuilder outputString = new StringBuilder(),
                    stackEvolution = new StringBuilder();
            int countIterations = 0;

            try {
                Problem problem = Problem.parseFromFile(filename,addImplicit);
                PlannerStack stack = new PlannerStack(problem.getInitialState(), problem.getGoalState(), heuristic);


                stackEvolution.append("\n# ITERATION ").append(countIterations++).append(":\n\n").append(stack.toString()).append("\n");

                try {
                    // We have to use step-by-step execution to get the full stack evolution
                    while (!stack.everythingDone()) {
                        stack.processTop();
                        stackEvolution.append("\n# ITERATION ").append(countIterations++).append(":\n\n").append(stack.toString()).append("\n");
                    }

                    outputString.append("Result: OK\n");


                } catch (CoffeeServerPlannerException ex) {
                    outputString.append("Result: FAIL\n");
                }

                outputString.append("\nFinal State:\n");
                outputString.append(stack.getCurrentState().toString()).append("\n");

                outputString.append("\nPerformed Operations:\n");
                Collection<Operation> ops = stack.getPerformedOperations();
                for (Operation op : ops) {
                    outputString.append(op.toString()).append("\n");
                }


                outputString.append("\n\n\nPlannerStack Evolution:\n\n").append(stackEvolution);
            } catch (IOException exception) {
                System.out.println("ERROR: Invalid file: "+filename);
                return;
            }

            if (output != null) {
                output.write(outputString.toString());
                output.close();
            } else {
                System.out.print(outputString.toString());
            }

        }



    }
}
