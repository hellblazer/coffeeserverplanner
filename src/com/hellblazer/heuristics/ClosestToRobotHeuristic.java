package com.hellblazer.heuristics;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;
import com.hellblazer.operations.Operation;
import com.hellblazer.predicates.*;
import com.hellblazer.state.State;

import java.util.Collections;
import java.util.Comparator;


/**
 * This Heuristic always tries to find correspondences that are closest to the robot's location
 * Created by burst on 04/11/2016.
 */
public class ClosestToRobotHeuristic extends Heuristic {


    private static RobotLocation findRobot(State state) {
        for (Predicate statePredicate : state) {
            if (statePredicate instanceof RobotLocation) {
                return (RobotLocation) statePredicate;
            }
        }
        return null;
    }

    public Predicate findCorrespondence(State state, Predicate predicate) {
        if (predicate instanceof Petition) {
            Petition petition = (Petition)predicate;
            if (!petition.getFstArgument().isBound()) {
                if (state.contains(new RobotLoaded(1))) petition.getSndArgument().setValue(1);
                else if (state.contains(new RobotLoaded(2))) petition.getSndArgument().setValue(2);
                else if (state.contains(new RobotLoaded(3))) petition.getSndArgument().setValue(3);
                return super.findCorrespondence(state,petition);
            }
            return super.findCorrespondence(state,predicate);
        } else if (!(predicate instanceof Machine || predicate instanceof Petition)) {
            // If it does not depend on the position, we don't have anything to do
            return super.findCorrespondence(state, predicate);
        } else {
            // We first search for the location of the robot, we don't have to check if that exists, as it is guaranteed
            RobotLocation location = ClosestToRobotHeuristic.findRobot(state);

            Predicate closestValid = null;
            int minDistance = 0;


            for (Predicate statePredicate : state) {
                if (predicate.canBind(statePredicate)) {
                    int distance = location.getFstArgument().getValue().dist(
                            ((BinaryPredicate<Office,Integer>)statePredicate).getFstArgument().getValue());
                    if (closestValid == null || minDistance > distance) {
                        closestValid=statePredicate;
                        minDistance=distance;
                    }
                }
            }

            return closestValid;
        }
    }

    /**
     * Class used in order to sort the goal PredicateList so that we get a sorted list that prioritizes the closest
     * requests.
     */
    private class ClosestFirst implements Comparator<Predicate> {

        private Office from;

        private int INCOMPARABLE_DIST = 10000;

        ClosestFirst(RobotLocation location) {
            this.from = location.getFstArgument().getValue();
        }

        @Override
        public int compare(Predicate o1, Predicate o2) {
            int o1dist = INCOMPARABLE_DIST, o2dist = INCOMPARABLE_DIST;
            // Robot location is always the last
            if (o1 instanceof RobotLocation) {
                o1dist=2*INCOMPARABLE_DIST;
            } else if (o1 instanceof RobotFree || o1 instanceof RobotLoaded) {
                o1dist = 2*INCOMPARABLE_DIST-1;
            } else if (o1 instanceof Served) {
                o1dist= ((Served)o1).getFstArgument().getValue().dist(this.from);
            }
            if (o2 instanceof RobotLocation) {
                o2dist=2*INCOMPARABLE_DIST;
            } else if (o2 instanceof RobotFree || o2 instanceof RobotLoaded) {
                o2dist = 2*INCOMPARABLE_DIST-1;
            } else if (o2 instanceof Served) {
                o2dist= ((Served)o2).getFstArgument().getValue().dist(this.from);
            }

            return o2dist-o1dist;
        }
    }


    public SortedPredicateList getGoal(State state, PredicateList predicates) {
        // We will first try to complete the tasks that are closest to the robot
        SortedPredicateList sorted = super.getGoal(state, predicates);
        Collections.sort(sorted, new ClosestFirst(ClosestToRobotHeuristic.findRobot(state)));
        return sorted;
    }

}
