package com.hellblazer.heuristics;

import com.hellblazer.operations.Operation;
import com.hellblazer.predicates.Predicate;
import com.hellblazer.predicates.PredicateList;
import com.hellblazer.predicates.SimplePredicateList;
import com.hellblazer.predicates.SortedPredicateList;
import com.hellblazer.state.State;

/**
 * This is the base heuristic class, there is no real heuristic here
 * The rest of heuristic classes have to extend this one.
 * Three different functions - that overlap in some cases - have been defined so that almost any complex heuristic
 * can be implemented without difficulties
 * Created by burst on 04/11/2016.
 */
public class Heuristic {

    /**
     * Finds a correspondence between the specified predicate and one in the state. This is just one place in which
     * the heuristics can be applied. A better match can be found.
     * @param state Current list of predicates
     * @param predicate Predicate to which a correspondence with the state's predicates have to be found
     * @return Predicate that can have a correspondence with the specified one. Null if none.
     */
    public Predicate findCorrespondence(State state, Predicate predicate) {
        // Just find the first correspondence
        return state.findCorrespondence(predicate);
    }

    /**
     * Gets the new goal of the stack (List of predicates that have to be checked) from an operation. In this method
     * two things can be optimised: the order of the predicates that will be added and the pre-instantiation that
     * some of the predicates can have. This can depend on the actual operation that is being performed. By default,
     * it uses the same heuristic defined in 'getGoal' as it was a global goal.
     * @param state Current list of predicates
     * @param operation Operation whose preconditions have to be checked
     * @return Preconditions of the operation
     */
    public SortedPredicateList getOperationGoal(State state, Operation operation) {
        // The new goal is just the preconditions, no instantiation is performed here
        PredicateList auxiliar = operation.getPreconditions();
        return this.getGoal(state, auxiliar);
    }

    /**
     * Gets the new goal of the stack (List of predicates that have to be checked) from an a list of predicates. In
     * this method two things can be optimised: the order of the predicates that will be added and the pre-instantiation
     * that some of the predicates can have.
     * @param state Current list of predicates
     * @param predicates Predicates that represent the goal to be checked
     * @return Predicates that represent the goal to be checked
     */
    public SortedPredicateList getGoal(State state, PredicateList predicates) {
        // The global goal is added as is, nothing fancy here
        if (predicates instanceof SortedPredicateList) {
            return (SortedPredicateList) predicates;
        } else {
            return new SimplePredicateList(predicates);
        }

    }
}
