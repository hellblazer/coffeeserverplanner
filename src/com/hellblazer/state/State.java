package com.hellblazer.state;

import com.hellblazer.misc.Office;
import com.hellblazer.predicates.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sprkrd on 10/16/16.
 */
public class State extends HashSet<Predicate> implements PredicateList{

    /**
     * Given a string representation of a state, returns the corresponding State object
     *
     * @param src String representation of a state
     * @return State object
     */
    public static State parseState(String src) {
        final Pattern pattern = Pattern.compile(
                "(?<predicate>[\\-A-Za-z]+)\\((?<args>(?:\\w+(?:,\\w+)*)?)\\)");

        State state = new State();

        Matcher matcher = pattern.matcher(src);

        while (matcher.find()) {
            String predicate = matcher.group("predicate");
            String[] args = matcher.group("args").split(",");
            switch (predicate) {
                case "Robot-free":
                    state.add(new RobotFree());
                    break;
                case "Robot-location":
                    state.add(new RobotLocation(new Office(args[0])));
                    break;
                case "Machine":
                    state.add(new Machine(new Office(args[0]), Integer.valueOf(args[1])));
                    break;
                case "Petition":
                    state.add(new Petition(new Office(args[0]), Integer.valueOf(args[1])));
                    break;
                case "Served":
                    state.add(new Served(new Office(args[0])));
                    break;
                case "Steps":
                    state.add(new Steps(Integer.valueOf(args[0])));
                    break;
                case "Robot-loaded":
                    state.add(new RobotLoaded(Integer.valueOf(args[0])));
                    break;
                default:
                    System.err.println("Unknown predicate: " + predicate);
            }
        }

        return state;
    }

    /**
     * Checks that all the predicates are fully instantiated. Throws exception otherwise
     * @param predicates collection of predicates
     * @throws IllegalArgumentException if there is one or more non-instantiated predicates.
     */
    private void checkFullyInstanced(Collection<? extends Predicate> predicates) {
        for (Predicate predicate : predicates) {
            if (!predicate.isFullyInstantiated()) {
                throw new IllegalArgumentException("State cannot contain non-instantiated predicates");
            }
        }
    }

    /**
     * Creates a state with the given predicates
     * @param predicates Array of predicates
     * @throws IllegalArgumentException if there is one or more non-instantiated predicates.
     */
    public State(Predicate... predicates) throws IllegalArgumentException {
        List<Predicate> listPredicates = Arrays.asList(predicates);
        checkFullyInstanced(listPredicates);
        addAll(listPredicates);
    }

    /**
     * Creates a state with the given predicates
     * @param predicates Collection of predicates
     * @throws IllegalArgumentException if there is one or more non-instantiated predicates.
     */
    public State(Collection<? extends Predicate> predicates) throws IllegalArgumentException {
        checkFullyInstanced(predicates);
        addAll(predicates);
    }

    /**
     * Looks for a predicate that can unify with a given predicate
     * @param inputPredicate input predicate
     * @return Predicate from this State that can unify with the given input predicate
     */
    public Predicate findCorrespondence(Predicate inputPredicate) {
        for (Predicate predicate : this) {
            if (inputPredicate.canBind(predicate)) {
                return predicate;
            }
        }
        return null;
    }

    /**
     * Override toString method so the format coincides with that of the
     * input text files.
     * @return
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Predicate predicate : this) {
            builder.append(predicate).append(';');
        }
        return builder.toString();
    }
}
