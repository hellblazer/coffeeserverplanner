package com.hellblazer.state;

import com.hellblazer.misc.Office;
import com.hellblazer.predicates.*;

import java.io.*;
import java.util.Random;

/**
 * Represents a problem. It groups together two stages: the initial one and
 * the target (objective) State.
 *
 * Created by sprkrd on 10/22/16.
 *
 * @see State
 */
public class Problem implements Cloneable {
    private final State initialState;
    private final State goalState;

    /**
     * Parse state from string. The string must follow the next format:
     * InitialState=Robot-location(o1);Machine(o4,3);Machine(o8,1);
     * Machine(o21,2);Machine(o23,1);Machine(o31,2);Petition(o3,1);
     * Petition(o11,3);Petition(o12,1);Petition(o13,2);Petition(o25,1);
     *
     * GoalState=Robot-location(o7);Served(o3);Served(o11);Served(o12);
     * Served(o13);Served(o25);
     * @param problemStr String that represents the problem.
     * @param implicitPredicates Add Step(0) and RobotFree() predicates to the initial state
     *                           even if they are not already present.
     * @return Parsed problem
     */
    public static Problem parse(String problemStr, boolean implicitPredicates) {
        int idx0 = problemStr.indexOf("InitialState=");
        int idx1 = problemStr.indexOf("GoalState=");

        String initialStr, goalStr;

        if (idx0 < idx1) {
            initialStr = problemStr.substring(idx0,idx1);
            goalStr = problemStr.substring(idx1);
        }
        else {
            initialStr = problemStr.substring(idx0);
            goalStr = problemStr.substring(idx1,idx0);
        }

        State initialState = State.parseState(initialStr);
        State goalState = State.parseState(goalStr);

        /* Add Steps(0) and RobotFree whether applicable. */
        if (implicitPredicates) {
            initialState.add(new Steps(0));
            initialState.add(new RobotFree());
        }

        return new Problem(initialState, goalState);
    }

    /**
     * Parse problem from a plain text file.
     * @param filename Path to file. This can be an absolute or relative path. If no file
     *                 can be found, the method assumes path relative to the com.hellblazer.benchmark
     *                 package.
     * @param implicitPredicates Add Step(0) and RobotFree() predicates to the initial state
     *                           even if they are not already present.
     * @return Parsed problem
     * @throws IOException
     */
    public static Problem parseFromFile(String filename, boolean implicitPredicates) throws IOException {
        StringBuilder builder = new StringBuilder();
        File file = new File(filename);
        /* Try to create input stream from filename */
        InputStream istream = istream = new FileInputStream(filename);
        int c;
        while ((c=istream.read())!=-1) {
            builder.appendCodePoint(c);
        }
        istream.close();
        return parse(builder.toString(), implicitPredicates);
    }

    /**
     * Utility method to generate random problems automatically.
     *
     * @param nMachines Number of machines in the generated problem.
     * @param nPetitions Number of petitions in the generated problem
     * @return Randomly generated problem. The problem is not guaranteed
     * to have a solution (intended).
     */
    public static Problem randomProblem(int nMachines, int nPetitions) {
        Random rand = new Random();
        State initialState = new State();
        State goalState = new State();
        /* Create an array with all the Offices and then shuffle it. Then
        * select the offices of the machines and the petitions by extracting
        * the next office from the array. This done in order to avoid repeated
        * offices. */
        Office[] offices = new Office[Office.N_OFFICES];
        for (int idx = 0; idx < offices.length; ++idx) {
            offices[idx] = new Office(idx+1);
        }
        /* Shuffle offices */
        for (int idx = 0; idx < offices.length; ++idx) {
            int jdx = rand.nextInt(offices.length);
            /* swap */
            Office tmp = offices[idx];
            offices[idx] = offices[jdx];
            offices[jdx] = tmp;
        }
        int nextOfficeIdx = 0;
        for (int idx = 0; idx < nMachines; ++idx) {
            int ncups = 1+rand.nextInt(3);
            initialState.add(new Machine(offices[nextOfficeIdx++],ncups));
        }
        for (int idx = 0; idx < nPetitions; ++idx) {
            int ncups = 1 + rand.nextInt(3);
            initialState.add(new Petition(offices[nextOfficeIdx],ncups));
            goalState.add(new Served(offices[nextOfficeIdx++]));
        }
        /* Add robot location at the beginning and desired robot location at the end.
         * There is no problem in these locations can being in the same office than
         * a machine or a petition. */
        Office randomStart = new Office(1+rand.nextInt(Office.N_OFFICES));
        Office randomEnd = new Office(1+rand.nextInt(Office.N_OFFICES));
        initialState.add(new RobotLocation(randomStart));
        goalState.add(new RobotLocation(randomEnd));
        return new Problem(initialState,goalState);
    }

    /**
     * Creates new problem with the given initial and goal state.
     * @param initialState Initial state of the problem.
     * @param goalState Goal state of the problem.
     */
    public Problem(State initialState, State goalState) {
        this.initialState = initialState;
        this.goalState = goalState;
    }

    /**
     * Getter
     * @return Initial state of the problem
     */
    public State getInitialState() {
        return initialState;
    }

    /**
     * Getter
     * @return Goal state of the problem
     */
    public State getGoalState() {
        return goalState;
    }

    @Override
    public Object clone() {
        return new Problem((State)initialState.clone(),(State)goalState.clone());
    }

    /**
     * Overrides toString method so that the output format coincides with that
     * of the input text files.
     * @return Formatted string encoding the problem. This string is guaranteed to
     * be parseable by the static parse method of this class.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("InitialState=").append(initialState)
            .append("\n\n").append("GoalState=").append(goalState)
            .append('\n');
        return builder.toString();
    }
}
