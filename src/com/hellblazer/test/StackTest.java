package com.hellblazer.test;

import com.hellblazer.heuristics.ClosestToRobotHeuristic;
import com.hellblazer.misc.CoffeeServerPlannerException;
import com.hellblazer.operations.Operation;
import com.hellblazer.predicates.*;
import com.hellblazer.stack.PlannerStack;
import com.hellblazer.state.State;

import java.util.Collection;

/**
 * Simple test for the STRIPS stack
 * Created by burst on 24/10/2016.
 */
public class StackTest {
    public static void main(String[] args) {
        State initialState = new State(
                new RobotFree(),
                new RobotLocation(1),
                new Petition(2,3),
                new Steps(0),
                new Machine(3,3));
        State goalState = new State(
                new Served(2),
                new RobotFree());

        PlannerStack stack = new PlannerStack(initialState, goalState, new ClosestToRobotHeuristic());


        System.out.println(stack.toString());

        try {

            while (!stack.everythingDone()) {

                stack.processTop();

                System.out.println(stack.toString());
            }

            System.out.println("Final State:");
            System.out.println(stack.getCurrentState().toString());

            System.out.println("Performed Operations:");
            Collection<Operation> ops = stack.getPerformedOperations();
            for (Operation op : ops) {
                System.out.println(op.toString());
            }

        } catch (CoffeeServerPlannerException ex) {
            ex.printStackTrace();
        }
    }
}
