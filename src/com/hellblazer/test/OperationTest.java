package com.hellblazer.test;

import com.hellblazer.misc.Office;
import com.hellblazer.operations.MakeCoffee;
import com.hellblazer.operations.MoveRobot;
import com.hellblazer.operations.ServeCoffee;
import com.hellblazer.predicates.*;
import com.hellblazer.state.State;

/**
 * Module for testing the Operations
 *
 * Created by sprkrd on 10/19/16.
 */
public class OperationTest {
    public static void main(String[] args) {
        State state = new State(
                new RobotFree(),
                new RobotLocation(1),
                new Petition(2,3),
                new Machine(3,3),
                new Steps(0));

        System.out.println("state: " + state);

        MoveRobot op1 = new MoveRobot();
        //System.out.println(op1);

        op1.getFstArgument().setValue(new Office(1));
        op1.getSndArgument().setValue(new Office(3));

        System.out.println("state after " + op1 + ": " + op1.apply(state));

        MakeCoffee op2 = new MakeCoffee();
        op2.getFstArgument().setValue(new Office(3));
        op2.getSndArgument().setValue(3);

        System.out.println("state after " + op2 + ": " + op2.apply(state));

        MoveRobot op3 = new MoveRobot();
        //System.out.println(op3);

        op3.getFstArgument().setValue(new Office(3));
        op3.getSndArgument().setValue(new Office(2));

        System.out.println("state after " + op3 + ": " + op3.apply(state));

        ServeCoffee op4 = new ServeCoffee();
        //System.out.println(op4);

        op4.getFstArgument().setValue(new Office(2));
        op4.getSndArgument().setValue(3);

        System.out.println("state after " + op4 + ": " + op4.apply(state));

        System.out.println("Applying op4 again (should fail): " + op4.apply(state));
    }
}
