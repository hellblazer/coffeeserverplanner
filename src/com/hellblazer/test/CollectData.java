package com.hellblazer.test;

import com.hellblazer.heuristics.ClosestToRobotHeuristic;
import com.hellblazer.predicates.Predicate;
import com.hellblazer.predicates.RobotFree;
import com.hellblazer.predicates.Steps;
import com.hellblazer.stack.PlannerStack;
import com.hellblazer.state.Problem;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Execute all benchmarks and compare to the base case (With no heuristic)
 * Created by burst on 04/11/2016.
 */
public class CollectData {
    /**
     * Gets the steps that had to be taken by the robot in order to find a solution
     * @param stack Executed stack
     * @return Number of steps
     */
    private static int getSteps(PlannerStack stack) {
        for (Predicate statePredicate : stack.getCurrentState()) {
            if (statePredicate instanceof Steps) {
                return ((Steps) statePredicate).getFstArgument().getValue();
            }
        }
        return 0;
    }

    public static void main(String[] args) throws IOException {
        PrintWriter out = new PrintWriter("data.csv");
        //out.println("nMachines,nPetitions,instance,improvement");
        for (int nMachines = 1; nMachines<=10; ++nMachines) {
            for (int nPetitions = 1; nPetitions<=10; ++nPetitions) {
                for (int idx = 0; idx < 100; ++idx) {
                    Problem problem = Problem.randomProblem(nMachines,nPetitions);
                    Problem clone = (Problem)problem.clone();
                    problem.getInitialState().add(new RobotFree());
                    problem.getInitialState().add(new Steps(0));
                    clone.getInitialState().add(new RobotFree());
                    clone.getInitialState().add(new Steps(0));
                    PlannerStack stack = new PlannerStack(problem.getInitialState(), problem.getGoalState(), new ClosestToRobotHeuristic());
                    PlannerStack base = new PlannerStack(clone.getInitialState(), clone.getGoalState());
                    stack.processStack();
                    base.processStack();
                    if (stack.everythingDone() && base.everythingDone()) {
                        double improvement = 1 - ((double) getSteps(stack)) / getSteps(base);
                        String row = nMachines + "," + nPetitions + ","  + idx + "," + improvement;
                        out.println(row);
                    }
                }
            }
        }
        out.close();
    }
}
