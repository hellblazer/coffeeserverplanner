package com.hellblazer.test;

import com.hellblazer.heuristics.ClosestToRobotHeuristic;
import com.hellblazer.stack.PlannerStack;
import com.hellblazer.predicates.Predicate;
import com.hellblazer.predicates.Steps;
import com.hellblazer.state.Problem;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Execute all benchmarks and compare to the base case (With no heuristic)
 * Created by burst on 04/11/2016.
 */
public class ProblemsTest {

    /**
     * Auxiliary class to compute the results of the Planner execution
     */
    static class ExecutionResult {

        private File file;
        private PlannerStack baseline;
        private PlannerStack withHeuristic;

//        PlannerStack stack = new PlannerStack(problem.getInitialState(), problem.getGoalState(), new ClosestToRobotHeuristic());

        /**
         * Default Constructor
         * @param file Problem file
         * @param baseline  Executed stack without heuristics
         * @param withHeuristic Executed stack with heuristics
         */
        ExecutionResult(File file, PlannerStack baseline, PlannerStack withHeuristic) {
            this.file=file;
            this.baseline=baseline;
            this.withHeuristic=withHeuristic;
        }

        /**
         * Gets the steps that had to be taken by the robot in order to find a solution
         * @param stack Executed stack
         * @return Number of steps
         */
        private int getSteps(PlannerStack stack) {
            for (Predicate statePredicate : stack.getCurrentState()) {
                if (statePredicate instanceof Steps) {
                    return ((Steps) statePredicate).getFstArgument().getValue();
                }
            }
            return 0;
        }

        /**
         * Informs whether or not the execution with heuristics is coherent with the execution without heuristics
         * @return Is coherent
         */
        boolean isCorrect() {
            return baseline.everythingDone() == withHeuristic.everythingDone();
        }

        /**
         * Informs whether or not the problem can be resolved
         * @return Can be resolved
         */
        boolean canBeResolved() {
            return baseline.everythingDone();
        }

        /**
         * Gets the relative amount of steps taken in the problem with heuristics compared to them in the problem
         * without.
         * @return Relative number of steps
         */
        double getRelativeSteps() {
            int baselineSteps = getSteps(baseline),
                    heuristicSteps = getSteps(withHeuristic);
            if (baselineSteps == 0) {
                // We don't want to divide by 0
                return 0;
            } else {
                return ((double) heuristicSteps)/baselineSteps;
            }
        }

        /**
         * Prints a String explaining the obtained result
         * @return Explanatory string
         */
        public String toString() {
            StringBuilder builder = new StringBuilder();

            // Print the file path
            builder.append(file.getAbsolutePath());
            builder.append("\t");

            // Print if it has solution
            builder.append(this.canBeResolved()? "RESOLVABLE" : "UNRESOLVABLE");
            builder.append("\t");

            // Print if it found the expected solution
            builder.append(this.isCorrect()? "OK": "FAILED");
            builder.append("\t");

            // Print the improvement percentage
            builder.append(this.canBeResolved()?(String.format("%.1f%%",100*(getRelativeSteps()-1))): "#");

            return builder.toString();
        }

        /**
         * List of available average modes
         */
        enum MeanMode {
            ARITHMETIC, GEOMETRIC, QUADRATIC, MEDIAN
        }

        /**
         * Gets a mean of the relative steps taken when comparing the execution with and without heuristics
         * @param results Collection of results that will be averaged
         * @param mode Mean mode that will be used
         * @return Mean relative steps
         */
        static double meanRelativeSteps(Collection<ExecutionResult> results, MeanMode mode) {
            double sum = 0, totalValid=0;
            if (results.isEmpty()) {
                return sum;
            }
            switch (mode) {
                case ARITHMETIC:
                    for (ExecutionResult result : results) {
                        if (result.canBeResolved() && result.isCorrect()) {
                            sum+=(result.getRelativeSteps());
                            totalValid++;
                        }
                    }
                    return sum/totalValid;

                case GEOMETRIC:
                    for (ExecutionResult result : results) {
                        if (result.canBeResolved() && result.isCorrect()) {
                            sum += Math.log(result.getRelativeSteps());
                            totalValid++;
                        }
                    }
                    return Math.exp(sum/totalValid);
                case QUADRATIC:
                    for (ExecutionResult result : results) {
                        if (result.canBeResolved() && result.isCorrect()) {
                            sum+=result.getRelativeSteps()*result.getRelativeSteps();
                            totalValid++;
                        }
                    }
                    return Math.sqrt(sum/totalValid);
                case MEDIAN:
                    ArrayList<Double> relSteps = new ArrayList<>();
                    for (ExecutionResult result : results) {
                        if (result.canBeResolved() && result.isCorrect()) {
                            relSteps.add(result.getRelativeSteps());
                        }
                    }
                    Collections.sort(relSteps);
                    return relSteps.get(relSteps.size()/2);
                default:
                    return 0;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        LinkedList<ExecutionResult> results = new LinkedList<>();

        File folder = new File("./benchmark");
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null) Arrays.sort(listOfFiles);

        for (File file : listOfFiles) {
            if (file.isFile()) {
                // System.out.println("Checking "+file.getAbsolutePath());
                try {
                    // It is read two times as the states are references
                    Problem problem = Problem.parseFromFile(file.getAbsolutePath(),true),
                            problemClone = Problem.parseFromFile(file.getAbsolutePath(),true);
                    // System.out.println(problem.getInitialState());

                    PlannerStack stack = new PlannerStack(problem.getInitialState(), problem.getGoalState(), new ClosestToRobotHeuristic()),
                            baseStack = new PlannerStack(problemClone.getInitialState(), problemClone.getGoalState());

                    stack.processStack();
                    baseStack.processStack();

                    results.push(new ExecutionResult(file, baseStack, stack));

                } catch (IOException exception) {
                    System.err.println("ERROR | IOException in file "+file.getAbsolutePath());
                }
            }
        }

        System.out.println("------------------------------------------------------------------------");
        System.out.println("RESULTS:");
        System.out.println("------------------------------------------------------------------------");
        for (ExecutionResult result: results) {
            System.out.println(result.toString());
        }

        System.out.println("------------------------------------------------------------------------");
        System.out.println("AVERAGE STEP NUMBER DIFFERENCES:");
        System.out.println("------------------------------------------------------------------------");
        System.out.println("ARITHMETIC: "+(String.format("%.1f%%",100*(ExecutionResult.meanRelativeSteps(results, ExecutionResult.MeanMode.ARITHMETIC)-1))));
        System.out.println("GEOMETRIC: "+(String.format("%.1f%%",100*(ExecutionResult.meanRelativeSteps(results, ExecutionResult.MeanMode.GEOMETRIC)-1))));
        System.out.println("QUADRATIC: "+(String.format("%.1f%%",100*(ExecutionResult.meanRelativeSteps(results, ExecutionResult.MeanMode.QUADRATIC)-1))));
        System.out.println("MEDIAN: "+(String.format("%.1f%%",100*(ExecutionResult.meanRelativeSteps(results, ExecutionResult.MeanMode.MEDIAN)-1))));

    }
}
