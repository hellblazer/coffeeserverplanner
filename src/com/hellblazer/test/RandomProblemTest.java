package com.hellblazer.test;

import com.hellblazer.state.Problem;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * This module creates a bunch of test instances and saves them into
 * the ./benchmark folder. The name of the files follow this convention:
 * instance_[nMachines]_[nPetitions]_[inst].in
 *
 * Created by sprkrd on 10/24/16.
 */
public class RandomProblemTest {
    public static void main(String[] args) {
        String basename = "benchmark/";
        /* Create several random problems for benchmark. */
        System.out.println("Creating random instances in benchmark/...");
        for (int nMachines = 1; nMachines <= 9; ++nMachines) {
            for (int nPetitions = 1; nPetitions <= 9; ++nPetitions) {
                for (int k = 0; k <= 9; ++k) {
                    String filename = basename +
                            String.format("instance_%d_%d_%d.in", nMachines, nPetitions, k);
                    try (PrintWriter writer = new PrintWriter(filename)) {
                        writer.print(Problem.randomProblem(nMachines, nPetitions));
                        writer.flush();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
            }
        }
        System.out.println("Done!");
    }
}
