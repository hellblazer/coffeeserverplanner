package com.hellblazer.test;

import com.hellblazer.misc.Argument;

/**
 * Module for testing the Argument class
 *
 * Created by sprkrd on 10/16/16.
 */
public class ArgumentTest {
    private static void checkSingleArgument(Argument<?> arg, String header) {
        System.out.println(header);
        System.out.println("  .getId(): " + arg.getId());
        System.out.println("  .getValue(): " + arg.getValue());
        System.out.println("  .hashCode(): " + arg.hashCode());
        System.out.println("  .isBound(): " + arg.isBound());
        System.out.println("  .toString(): " + arg.toString());
        System.out.println();
    }

    private static void checkInteraction(Argument<?> arg1, Argument<?> arg2, String header) {
        System.out.println(header);
        System.out.println("  .equals() " + arg1.equals(arg2));
        System.out.println("  .boundAndEqual(): " + arg1.boundAndEqual(arg2));
        System.out.println();
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        System.out.println("Create unbound argument");
        Argument<Object> arg1 = new Argument<>();
        checkSingleArgument(arg1, "arg1");

        System.out.println("Create bound argument");
        Argument<Object> arg2 = new Argument<Object>(42);
        checkSingleArgument(arg2, "arg2");

        System.out.println("Check interaction between bound and unbound argument");
        checkInteraction(arg1,arg2,"arg1 vs arg2");

        System.out.println("Clone arg2 and check");
        Argument<Object> arg3 = (Argument<Object>)arg2.clone();
        checkSingleArgument(arg3, "arg3 (arg2 clone)");
        checkInteraction(arg2,arg3,"arg2 vs arg3");

        System.out.println("Bind arg3 to a different value and check that there is not aliasing");
        arg3.setValue(24);
        checkSingleArgument(arg2,"arg2");
        checkSingleArgument(arg3,"arg3 (after binding with 24)");
        checkInteraction(arg2,arg3,"arg2 vs arg3");

        System.out.println("Unbind arg3 and compare to arg1");
        arg3.setValue(null);
        checkSingleArgument(arg3,"arg3 (after unbinding)");
        checkInteraction(arg1,arg3,"arg1 vs arg3");

        System.out.println("Bind arg1 to a different type and recheck");
        arg1.setValue("Different type");
        checkSingleArgument(arg1, "arg1 (after binding different type)");
        checkInteraction(arg1,arg3, "arg1 vs arg3");
    }
}
