package com.hellblazer.test;

import com.hellblazer.predicates.*;
import com.hellblazer.state.State;

/**
 * Module to test the State class.
 *
 * Created by sprkrd on 10/19/16.
 */
public class StateTest {
    private static void testCorrespondence(State state, Predicate predicate) {
        System.out.println("Correspondence for " + predicate + "" +
                " in state: " + state.findCorrespondence(predicate));
    }

    public static void main(String[] args) {
        State state = new State(
                new RobotFree(),
                new RobotLocation(1),
                new Petition(2,3),
                new Machine(3,3));

        System.out.println("state: " + state);

        System.out.println("Add (assert) Petition(o4,2) and Machine(o5,2)");
        state.add(new Petition(4,2));
        state.add(new Machine(5,2));
        System.out.println(state);

        System.out.println("Add Machine(o5,2) again (duplicate predicates should not appear)");
        state.add(new Machine(5,2));
        System.out.println(state);

        System.out.println("Remove (retract) predicate Petition(o4,2)");
        state.remove(new Petition(4,2));
        System.out.println(state);

        System.out.println("Retract non-present predicate. The state should not change");
        state.remove(new Served(1));
        System.out.println(state);

        System.out.println("Testing findCorrespondence");
        testCorrespondence(state, new Petition(null,3));
        testCorrespondence(state, new RobotFree());
        testCorrespondence(state, new Served(2));
        testCorrespondence(state, new Machine());
    }
}
