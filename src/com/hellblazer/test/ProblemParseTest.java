package com.hellblazer.test;

import com.hellblazer.state.Problem;

import java.io.IOException;

/**
 * Module that tests the parser. First it tries to parse from a string and
 * then from a file.
 *
 * Created by sprkrd on 10/23/16.
 */
public class ProblemParseTest {
    public static void main(String[] args) throws IOException {
        String str = "InitialState=Robot-location(o1);Machine(o4,3);Machine(o8,1);\n" +
                "Machine(o21,2);Machine(o23,1);Machine(o31,2);Petition(o3,1);\n" +
                "Petition(o11,3);Petition(o12,1);Petition(o13,2);Petition(o25,1);\n\n" +
                "GoalState=Robot-location(o7);Served(o3);Served(o11);Served(o12);\n" +
                "Served(o13);Served(o25);\n";

        Problem problem = Problem.parse(str,true);

        System.out.println(problem);

        Problem problem2 = Problem.parseFromFile("benchmark/instance_4_3_4.in",true);
        System.out.println(problem2);
    }
}
