package com.hellblazer.test;

import com.hellblazer.heuristics.ClosestToRobotHeuristic;
import com.hellblazer.heuristics.Heuristic;
import com.hellblazer.misc.CoffeeServerPlannerException;
import com.hellblazer.operations.Operation;
import com.hellblazer.stack.PlannerStack;
import com.hellblazer.state.Problem;
import com.hellblazer.state.State;

import java.io.IOException;
import java.util.Collection;

/**
 * Module that tests the parsing and execution capabilities combined
 *
 * Created by burst on 24/10/2016.
 */
public class ProblemExecuteTest {
    public static void main(String[] args) throws IOException {
        Problem problem = Problem.parseFromFile("benchmark/contrived/contrived_3.in",false);

        System.out.println(problem.getInitialState());

        //PlannerStack stack = new PlannerStack(problem.getInitialState(), problem.getGoalState());
        PlannerStack stack = new PlannerStack(problem.getInitialState(), problem.getGoalState(), new ClosestToRobotHeuristic());


        System.out.println(stack.toString());

        if (stack.processStack()) {
            System.out.println("SUCCESS!");
        } else {
            System.out.println("FAILED!");
            System.out.println("Unsatisfied PlannerStack:");
            System.out.println(stack.toString());
        }

        System.out.println("Final State:");
        System.out.println(stack.getCurrentState().toString());

        System.out.println("Performed Operations:");
        Collection<Operation> ops = stack.getPerformedOperations();
        for (Operation op : ops) {
            System.out.println(op.toString());
        }
    }
}
