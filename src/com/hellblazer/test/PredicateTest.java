package com.hellblazer.test;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;
import com.hellblazer.predicates.*;

/**
 * Module for testing the Predicate class
 *
 * Created by sprkrd on 10/16/16.
 */
public class PredicateTest {
    private static void checkSinglePredicate(Predicate predicate, String header) {
        System.out.println(header);
        System.out.println("  .isFullyInstantiated(): " + predicate.isFullyInstantiated());
        System.out.println("  .hashCode(): " + predicate.hashCode());
        System.out.println("  .toString(): " + predicate.toString());
        System.out.println();
    }

    private static void checkInteraction(Predicate predicate1, Predicate predicate2, String header) {
        System.out.println(header);
        System.out.println("  .canBind(): " + predicate1.canBind(predicate2));
        System.out.println("  .equals(): " + predicate1.equals(predicate2));
        System.out.println();
    }

    public static void main(String[] args) {
        /* Create some offices */
        Office o1 = new Office(1);
        Office o2 = new Office(2);

        System.out.println("Test fully instantiated Machine predicate");
        Machine predicate1 = new Machine(o1,3);

        System.out.println("Check general methods");
        checkSinglePredicate(predicate1, "predicate1");

        System.out.println("Check get methods");
        System.out.println("predicate1");
        System.out.println("  .getFstArgument(): " + predicate1.getFstArgument());
        System.out.println("  .getSndArgument(): " + predicate1.getSndArgument());

        System.out.println("Test non-fully instantiated Machine predicate");
        Machine predicate2 = new Machine();
        checkSinglePredicate(predicate2, "predicate2");
        checkInteraction(predicate1, predicate2, "predicate1 vs predicate2");

        System.out.println("Set first argument of predicate2");
        predicate2.getFstArgument().setValue(o1);

        checkSinglePredicate(predicate2, "predicate2 (after setting location to o1)");
        checkInteraction(predicate1, predicate2, "predicate1 vs predicate2");

        System.out.println("Set nCups");
        predicate2.getSndArgument().setValue(3);
        checkSinglePredicate(predicate2, "predicate2 (after setting nCups to 3)");
        checkInteraction(predicate1, predicate2, "predicate1 vs predicate2");

        System.out.println("Change predicate2 location to o2");
        predicate2.getFstArgument().setValue(o2);
        checkSinglePredicate(predicate2,"predicate2 (after changing location to o2");
        checkInteraction(predicate1, predicate2, "predicate1 vs predicate2");

        System.out.println("Create more predicates");

        Petition predicate3 = new Petition();
        checkSinglePredicate(predicate3, "predicate3");
        predicate3.getFstArgument().setValue(o2);
        predicate3.getSndArgument().setValue(3);
        checkSinglePredicate(predicate3, "predicate3 (after setting location to o2 and nCups to 3)");

        Predicate predicate4 = new RobotFree();
        checkSinglePredicate(predicate4, "predicate4");

        Predicate predicate5 = new RobotLoaded(2);
        checkSinglePredicate(predicate5, "predicate5");

        Predicate predicate6 = new RobotLocation();
        checkSinglePredicate(predicate6, "predicate6");

        Predicate predicate7 = new RobotLocation(o1);
        checkSinglePredicate(predicate7, "predicate7");

        Predicate predicate8 = new Served(o1);
        checkSinglePredicate(predicate8, "predicate8");

        Predicate predicate9 = new Steps(15);
        checkSinglePredicate(predicate9, "predicate9");

        Predicate predicate10 = new Steps(new Argument<>(15));
        checkSinglePredicate(predicate10, "predicate10");

        Predicate predicate11 = new Steps(new Argument<>(14));
        checkSinglePredicate(predicate11, "predicate11");

        System.out.println("Test some additional interactions");
        checkInteraction(predicate3, predicate2, "predicate3 vs predicate2");
        checkInteraction(predicate4,predicate5,"predicate4 vs predicate5");
        checkInteraction(predicate4,predicate9,"predicate4 vs predicate9");
        checkInteraction(predicate9,predicate10,"predicate9 vs predicate10");
        checkInteraction(predicate9,predicate11,"predicate9 vs predicate11");

        System.out.println("Finally, test binding");
        Predicate predicate12 = new Machine();
        checkSinglePredicate(predicate12,"predicate12");

        System.out.println("Binding predicate12 to predicate1: "+predicate12.bind(predicate1));

        checkSinglePredicate(predicate12,"predicate12");
    }
}
