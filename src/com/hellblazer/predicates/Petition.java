package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;

/**
 * Petition predicate. It is a binary predicate. The first Argument
 * is the Office where the Petition is located, and the second Argument
 * is the number of requested cups of coffee.
 *
 * Created by sprkrd on 10/16/16.
 */
public class Petition extends BinaryPredicate<Office,Integer> {

    /**
     * No particularities w.r.t. the default BinaryPredicate constructor.
     * (i.e. creates Machine Predicate with both Arguments unbound).
     * @see BinaryPredicate
     */
    public Petition() {
        super();
    }

    /**
     * Takes the (unwrapped) arguments and creates a Petition
     * predicate with already bound parameters.
     *
     * @param office The Office where the Petition is located
     * @param ncups The number of requested cups
     *
     * @see BinaryPredicate
     */
    public Petition(Office office, int ncups) {
        super(office,ncups);
    }

    /**
     * Takes the (unwrapped) arguments and creates a Petition
     * predicate with already bound parameters.
     *
     * @param office As an integer (more convenient in some cases)
     * @param ncups Number of requested cups
     *
     * @see BinaryPredicate
     */
    public Petition(int office, int ncups) {
        super(new Office(office),ncups);
    }

    /**
     * Constructor that takes the already wrapped Arguments as input.
     *
     * @param office Office where the Petition is located
     * @param ncups Number of requested cups
     */
    public Petition(Argument<Office> office, Argument<Integer> ncups) {
        super(office, ncups);
    }
}
