package com.hellblazer.predicates;

import com.hellblazer.stack.Stackable;

/**
 * Interface implemented by all the Predicates of this problem.
 * It defines certain functionality that is common to all the
 * Predicates.
 *
 * Created by salva on 05/10/2016.
 *
 * @see BinaryPredicate
 * @see UnaryPredicate
 * @see NullaryPredicate
 */

public interface Predicate extends Stackable {
    /**
     * Returns whether the proposition is fully instantiated
     * @return true if fully instantiated
     */
    boolean isFullyInstantiated();

    /**
     * Tells whether this Predicate is fully instantiated and is the same than other
     * Predicate or whether the unbound Arguments of this predicate can be bound
     * so the two predicates coincide.
     * @param predicate Another predicate
     * @return true if the Arguments of this Predicate can be bound so the two
     * predicates coincide.
     */
    boolean canBind(Predicate predicate);

    /**
     * Bind the unbound Arguments of this Predicate so that at the end
     * this Predicate is equal to another given Predicate.
     * @param predicate Another Predicate
     * @return true if the unbound Arguments could be bound.
     */
    boolean bind(Predicate predicate);
}
