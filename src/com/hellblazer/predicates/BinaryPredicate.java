package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;

/**
 * Predicate that takes two arguments. This abstract class implements some
 * of the functionality that is common to all the predicates with
 * two arguments. Check the classes that inherit from this class for more
 * info.
 *
 * Created by sprkrd on 10/18/16.
 *
 * @see Machine
 * @see Petition
 */
public abstract class BinaryPredicate<T,U> implements Predicate, Cloneable {

    private Argument<T> fst;
    private Argument<U> snd;

    /**
     * Default constructor. Sets the parameters to non-instantiated arguments.
     */
    public BinaryPredicate() {
        fst = new Argument<>();
        snd = new Argument<>();
    }

    /**
     * Alternative constructor. Gets the first and second parameters as input
     * (directly, without the Argument wrapper).
     * @param fst First parameter
     * @param snd Second parameter
     */
    public BinaryPredicate(T fst, U snd) {
        this.fst = new Argument<>(fst);
        this.snd = new Argument<>(snd);
    }

    /**
     * Alternative constructor. Gets the first and second parameters with the
     * Argument wrapper
     * @param fst First parameter
     * @param snd Second parameter
     * @see Argument
     */
    public BinaryPredicate(Argument<T> fst, Argument<U> snd) {
        this.fst = fst;
        this.snd = snd;
    }

    /**
     * @return First parameter (with the Argument wrapper)
     */
    public Argument<T> getFstArgument() {
        return fst;
    }

    /**
     * @return Second parameter (with the Argument wrapper)
     */
    public Argument<U> getSndArgument() {
        return snd;
    }

    /**
     * @return Whether the predicate has been fully instantiated (i.e. both
     * Arguments are bound).
     */
    @Override
    public boolean isFullyInstantiated() {
        return fst.isBound() && snd.isBound();
    }

    /**
     * Overrides default behavior of equals so that a BinaryPredicate is
     * equal to another object iff that other object is the same type of
     * Predicate (i.e. same class) and the arguments of both this predicate
     * and the other one coincide).
     * @param o Another object
     * @return Whether this Predicate is equal to Object o.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BinaryPredicate<?, ?> that = (BinaryPredicate<?, ?>) o;

        return fst.equals(that.fst) && snd.equals(that.snd);

    }

    /**
     * Overrides hashCode in order to be coherent with equals method
     * @return hash code based on the class of this Predicate and its
     * Arguments.
     */
    @Override
    public int hashCode() {
        int result = getClass().hashCode();
        result = 31*result + fst.hashCode();
        result = 255*result + snd.hashCode();
        return result;
    }

    /**
     * Tells whether this Predicate is fully instantiated and is the same than other
     * Predicate or whether the unbound Arguments of this predicate can be bound
     * so the two predicates coincide.
     * @param predicate Another predicate
     * @return true if the Arguments of this Predicate can be bound so the two
     * predicates coincide.
     * @see Predicate
     */
    @Override
    public boolean canBind(Predicate predicate) {
        boolean canBindFst = false;
        boolean canBindSnd = false;
        if (getClass() == predicate.getClass()) {
            BinaryPredicate<?,?> bpredicate = (BinaryPredicate<?,?>)predicate;
            canBindFst = (fst.isBound()^bpredicate.fst.isBound()) ||
                    fst.boundAndEqual(bpredicate.fst);
            canBindSnd = (snd.isBound()^bpredicate.snd.isBound()) ||
                    snd.boundAndEqual(bpredicate.snd);
        }
        return canBindFst && canBindSnd;
    }

    /**
     * Bind the unbound Arguments of this Predicate so that at the end
     * this Predicate is equal to another given Predicate.
     * @param predicate Another Predicate
     * @return true if the unbound Arguments could be bound.
     */
    @Override
    @SuppressWarnings("unchecked") /* The casting is guaranteed to work since
    canBind() already checks that the two predicates belong to the same class.*/
    public boolean bind(Predicate predicate) {
        if (!canBind(predicate)) {
            return false;
        }
        BinaryPredicate<T,U> bpredicate = (BinaryPredicate<T,U>)predicate;
        if (!fst.isBound()) {
            fst.setValue(bpredicate.fst.getValue());
        }
        if (!snd.isBound()) {
            snd.setValue(bpredicate.snd.getValue());
        }
        return true;
    }

    /**
     * Overridden clone method that works correctly with instances of this
     * class.
     * @return Cloned BinaryPredicate
     * @throws CloneNotSupportedException If the cloning fails (should never happen)
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object clone() throws CloneNotSupportedException {
        BinaryPredicate<T,U> cloned = (BinaryPredicate<T,U>)super.clone();
        cloned.fst = (Argument<T>)fst.clone();
        cloned.snd = (Argument<U>)snd.clone();
        return cloned;
    }

    /**
     * It takes into account the concrete class of the instance in order
     * to create a meaningful string representation of the object.
     *
     * @return String representation of this predicate.
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() +
                '(' + fst + ',' + snd + ')';
    }
}
