package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;

/**
 * Predicate that takes one Argument. This abstract class implements some
 * of the functionality that is common to all the predicates with
 * one Argument. Check the classes that inherit from this class for more
 * info.
 *
 * Created by sprkrd on 10/18/16.
 *
 * @see RobotLoaded
 * @see RobotLocation
 * @see Served
 * @see Steps
 */
public abstract class UnaryPredicate<T> implements Predicate {
    private Argument<T> fst;

    /**
     * Default constructor. The only parameter of this predicate is left
     * unbound.
     */
    public UnaryPredicate() {
        fst = new Argument<>();
    }

    /**
     * Alternative constructor. Sets the value of the only parameter
     * to a given (unwrapped) Argument.
     * @param fst Unwrapped Argument value
     */
    public UnaryPredicate(T fst) {
        this.fst = new Argument<>(fst);
    }

    /**
     * Alternative constructor. Sets the value of the only parameter
     * to a given (wrapped) Argument.
     * @param fst Wrapped Argument
     */
    public UnaryPredicate(Argument<T> fst) {
        this.fst = fst;
    }

    /**
     * Getter of the first (and only) Argument
     * @return Wrapped argument
     */
    public Argument<T> getFstArgument() {
        return fst;
    }

    /**
     * Tells whether the (only) Argument of this Predicate is bound.
     * @return true if the Argument is bound.
     */
    @Override
    public boolean isFullyInstantiated() {
        return fst.isBound();
    }

    /**
     * Tells whether this Predicate is fully instantiated and is the same than other
     * Predicate or whether the unbound Argument of this predicate can be bound
     * so the two predicates coincide.
     * @param predicate Another predicate
     * @return true if the Argument of this Predicate can be bound so the two
     * predicates coincide.
     *
     * @see Predicate
     */
    @Override
    public boolean canBind(Predicate predicate) {
        boolean canBindFst = false;
        if (getClass()==predicate.getClass()) {
            UnaryPredicate<?> upredicate = (UnaryPredicate<?>) predicate;
            canBindFst = (fst.isBound()^upredicate.fst.isBound()) ||
                    fst.boundAndEqual(upredicate.fst);
        }
        return canBindFst;
    }

    /**
     * Bind the unbound Arguments of this Predicate so that at the end
     * this Predicate is equal to another given Predicate.
     * @param predicate Another Predicate
     * @return true if the unbound Arguments could be bound.
     *
     * @see Predicate
     */
    @Override
    @SuppressWarnings("unchecked") /* The casting is guaranteed to work since
    in canBind we check that the two predicates belong to the same class. */
    public boolean bind(Predicate predicate) {
        if (!canBind(predicate)) {
            return false;
        }
        UnaryPredicate<T> upredicate = (UnaryPredicate<T>) predicate;
        if (!fst.isBound()) {
            fst.setValue(upredicate.fst.getValue());
        }
        return true;
    }

    /**
     * Overrides default behavior of equals so that a UnaryPredicate is
     * equal to another object iff that other object is the same type of
     * Predicate (i.e. same class) and the Argument of both this predicate
     * and the other one coincide).
     * @param o Another object
     * @return Whether this Predicate is equal to Object o.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UnaryPredicate<?> that = (UnaryPredicate<?>) o;

        return fst.equals(that.fst);

    }

    /**
     * Overrides hashCode in order to be coherent with equals method
     * @return hash code based on the class of this Predicate and its
     * Arguments.
     */
    @Override
    public int hashCode() {
        int result = getClass().hashCode();
        result = 31*result + fst.hashCode();
        return result;
    }

    /**
     * Overridden clone method that works correctly with instances of this
     * class.
     * @return Cloned instance of UnaryPredicate
     * @throws CloneNotSupportedException If the cloning fails (should never happen)
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object clone() throws CloneNotSupportedException {
        UnaryPredicate<T> cloned = (UnaryPredicate<T>)super.clone();
        cloned.fst = (Argument<T>)fst.clone();
        return cloned;
    }

    /**
     * It takes into account the concrete class of the instance in order
     * to create a meaningful string representation of the object.
     *
     * @return String representation of this predicate.
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() +
                '(' + fst + ')';
    }
}
