package com.hellblazer.predicates;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Simple implementation for SortedPredicateList. It is a extension of LinkedList&lt;Predicate&gt;
 * Created by burst on 23/10/2016.
 */
public class SimplePredicateList extends LinkedList<Predicate> implements SortedPredicateList{


    /**
     * Convenience constructor
     * @param predicates Array (or multiple arguments) of Predicates
     */
    public SimplePredicateList(Predicate... predicates) {
        List<Predicate> listPredicates = Arrays.asList(predicates);
        addAll(listPredicates);
    }

    /**
     * Alternative constructor. Builds new list copying elements
     * of another PredicateList
     * @param list List to be copied.
     */
    public SimplePredicateList(PredicateList list) {
        addAll(list);
    }

}
