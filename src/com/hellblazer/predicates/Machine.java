package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;

/**
 * Machine predicate. It is a binary predicate. The first Argument
 * is the Office where the Machine is located, and the second Argument
 * is the number of cups the Machine offers.
 *
 * Created by sprkrd on 10/16/16.
 */
public class Machine extends BinaryPredicate<Office,Integer> {

    /**
     * No particularities w.r.t. the default BinaryPredicate constructor.
     * (i.e. creates Machine Predicate with both Arguments unbound).
     * @see BinaryPredicate
     */
    public Machine() {
        super();
    }

    /**
     * Takes the (unwrapped) arguments and creates a Machine
     * predicate with already bound parameters.
     *
     * @param office The Office where the Machine is located
     * @param ncups The number of cups the Machine is able to prepare.
     *
     * @see BinaryPredicate
     */
    public Machine(Office office, int ncups) {
        super(office,ncups);
    }

    /**
     * Takes the (unwrapped) arguments and creates a Machine
     * predicate with already bound parameters.
     *
     * @param office As an integer (more convenient in some cases)
     * @param ncups Number of cups that can be prepared in this Machine
     *
     * @see BinaryPredicate
     */
    public Machine(int office, int ncups) {
        super(new Office(office),ncups);
    }

    /**
     * Constructor that takes the already wrapped Arguments as input.
     *
     * @param office Office where the Machine is located.
     * @param ncups Number of cups that can be prepared in this Machine.
     */
    public Machine(Argument<Office> office, Argument<Integer> ncups) {
        super(office, ncups);
    }

}
