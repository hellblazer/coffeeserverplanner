package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;

/**
 * Steps predicates. Receives an Integer as its only Argument. Counts
 * the number of steps performed by the robot.
 *
 * Created by sprkrd on 10/16/16.
 */
public class Steps extends UnaryPredicate<Integer> {

    /**
     * Default constructor (with unbound Argument)
     */
    public Steps() {
        super();
    }

    /**
     * Alternative constructor. The number of steps is initialized
     * with the input (unwrapped) parameter
     *
     * @param steps Number of steps.
     */
    public Steps(int steps) {
        super(steps);
    }

    /**
     * Alternative constructor. The number of steps is initialized
     * with the input (wrapped) parameter.
     *
     * @param steps Number of steps (wrapped)
     */
    public Steps(Argument<Integer> steps) {
        super(steps);
    }

}
