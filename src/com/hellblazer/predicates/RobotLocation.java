package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;

/**
 * RobotLocation predicate. Has one parameter: the Office where
 * the robot is located.
 *
 * Created by sprkrd on 10/16/16.
 */
public class RobotLocation extends UnaryPredicate<Office> {

    /**
     * Default constructor (with unbound Argument)
     */
    public RobotLocation() {
        super();
    }

    /**
     * Alternative constructor. Creates a RobotLocation predicate
     * with an already bound parameter.
     * @param office Office. Initial value of the parameter (unwrapped)
     *
     * @see Office
     */
    public RobotLocation(Office office) {
        super(office);
    }

    /**
     * Alternative constructor. Creates a RobotLocation predicate
     * with an already bound parameter.
     * @param office Number of Office. Initial value of the parameter (unwrapped)
     */
    public RobotLocation(int office) {
        super(new Office(office));
    }

    /**
     * Alternative constructor. Creates a RobotLocation predicate
     * with an already bound parameter.
     * @param office Initial value of the parameter (wrapped)
     */
    public RobotLocation(Argument<Office> office) {
        super(office);
    }

    /**
     * Override toString so that the output format coincides with that of
     * the proposed input text files.
     * @return
     */
    @Override
    public String toString() {
        return "Robot-location(" + getFstArgument() + ')';
    }
}
