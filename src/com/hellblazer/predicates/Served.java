package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;

/**
 * Served predicate. Identifies those Offices where there was a Petition,
 * but now it is satisfied.
 *
 * Created by sprkrd on 10/16/16.
 */
public class Served extends UnaryPredicate<Office> {

    /**
     * Default constructor (with unbound Argument)
     */
    public Served() {
        super();
    }

    /**
     * Alternative constructor. Creates a Served predicate
     * with an already bound parameter.
     * @param office Office. Initial value of the parameter (unwrapped)
     *
     * @see Office
     */
    public Served(Office office) {
        super(office);
    }

    /**
     * Alternative constructor. Creates a Served predicate
     * with an already bound parameter.
     * @param office Number of Office. Initial value of the parameter (unwrapped)
     */
    public Served(int office) {
        super(new Office(office));
    }

    /**
     * Alternative constructor. Creates a Served predicate
     * with an already bound parameter.
     * @param office Initial value of the parameter (wrapped)
     */
    public Served(Argument<Office> office) {
        super(office);
    }

}
