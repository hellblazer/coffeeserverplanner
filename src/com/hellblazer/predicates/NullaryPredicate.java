package com.hellblazer.predicates;

/**
 * Nullary predicate takes two arguments. This abstract class implements some
 * of the functionality that is common to all the predicates with
 * no arguments. Check the classes that inherit from this class for more
 * info.
 *
 * Created by sprkrd on 10/18/16.
 *
 * @see RobotFree
 */
public abstract class NullaryPredicate implements Predicate {

    /**
     * Always true
     * @return true
     */
    @Override
    public boolean isFullyInstantiated() {
        return true;
    }

    /**
     * Only true iff the other Predicate belongs to the same class.
     * @param predicate Another predicate
     * @return Whether the other Predicate is also a NullaryPredicate of the
     * same type
     */
    @Override
    public boolean canBind(Predicate predicate) {
        return equals(predicate);
    }

    /**
     * Does nothing, since there are no Arguments to bind.
     * @param predicate Another predicate
     * @return Same than equals
     */
    @Override
    public boolean bind(Predicate predicate) {
        return equals(predicate);
    }

    /**
     * True only if the other object is a NullaryPredicate with the same concrete task.
     * @param o Another object
     * @return Whether the other object belongs to the same concrete task than this
     * Predicate.
     */
    @Override
    public boolean equals(Object o) {
        return (o!=null) && (getClass()==o.getClass());
    }

    /**
     * Overrides hashCode to be coherent with equals method.
     * @return hashCode based on the concrete class of the Predicate.
     */
    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    /**
     * String representation that takes into account the concrete class
     * of this object.
     * @return String representation of this NullaryPredicate
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + "()";
    }
}
