package com.hellblazer.predicates;

import java.util.List;

/**
 * Defines a PredicateList in which order matters (semantic distinction, no difference
 * in terms of implementation)
 * Created by burst on 04/11/2016.
 */
public interface SortedPredicateList extends PredicateList, List<Predicate> {
}
