package com.hellblazer.predicates;

/**
 * RobotFree predicate. Indicates that the robot does not hold any
 * cup of coffee.
 *
 * Created by sprkrd on 10/16/16.
 */
public class RobotFree extends NullaryPredicate {
    /**
     * Override toString so that the output format coincides with that of
     * the proposed input text files.
     * @return
     */
    @Override
    public String toString() {
        return "Robot-free()";
    }
}
