package com.hellblazer.predicates;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.hellblazer.stack.Stackable;

/**
 * Interface that represents a collection of predicates.
 * Created by salva on 05/10/2016.
 */
public interface PredicateList extends Collection<Predicate>,  Stackable {

}
