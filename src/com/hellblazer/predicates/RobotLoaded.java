package com.hellblazer.predicates;

import com.hellblazer.misc.Argument;

/**
 * RobotLoaded predicate. Tells how many cups (between 1 and 3)
 * the robot is holding.
 *
 * Created by sprkrd on 10/16/16.
 */
public class RobotLoaded extends UnaryPredicate<Integer> {

    /**
     * Default constructor (with unbound Argument)
     */
    public RobotLoaded() {
        super();
    }

    /**
     * Alternative constructor. Creates a RobotLoaded predicate
     * with an already bound parameter.
     * @param ncups Initial value of the parameter (unwrapped)
     */
    public RobotLoaded(int ncups) {
        super(ncups);
    }

    /**
     * Alternative constructor. Creates a RobotLoaded predicate
     * with an already bound parameter.
     * @param ncups Initial value of the parameter (wrapped)
     */
    public RobotLoaded(Argument<Integer> ncups) {
        super(ncups);
    }

    /**
     * Override toString so that the output format coincides with that of
     * the proposed input text files.
     * @return
     */
    @Override
    public String toString() {
        return "Robot-loaded(" + getFstArgument() + ')';
    }
}
