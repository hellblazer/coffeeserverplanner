package com.hellblazer.misc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Simple wrapper around an integer value. It provides basic
 * functionality to compute the Manhattan distance between Offices
 * in a convenient way.
 *
 * Created by sprkrd on 10/16/16.
 */
public class Office {
    /** Number of rows **/
    public final static int M = 6;
    /** Number of columns **/
    public final static int N = 6;
    /** Number of offices **/
    public final static int N_OFFICES = N*M; // Number of offices

    private final Pattern officePattern = Pattern.compile("o(?<number>[0-9]+)");

    private final int o;

    /**
     * Creates an office from a String representation.
     * @param ostr String representation of the office. Format o[number] (e.g. o1, o15, o36)
     * @throws IllegalArgumentException In case the given String does not follow the expected format.
     */
    public Office(String ostr) throws IllegalArgumentException {
        Matcher matcher = officePattern.matcher(ostr);
        if (matcher.matches()) {
            this.o = Integer.valueOf(matcher.group("number"));
        }
        else {
            throw new IllegalArgumentException(String.format(
                    "Invalid office string (%s). Correct format is o[0-9]+",ostr));
        }
    }

    /**
     * Office with given number
     * @param o Number of the office
     */
    public Office(int o) {
        this.o = o;
    }

    /**
     * Office with given coordinates. The coordinate (0,0) is the office
     * at the top left.
     * @param x Horizontal coordinate
     * @param y Vertical coordinate
     */
    public Office(int x, int y) {
        o = 1 + y*M + x;
    }

    /**
     * Getter of the office number.
     * @return Number of the office (between 1 and Office.N_OFFICES)
     */
    public int getOfficeNumber() {
        return o;
    }

    /**
     * Getter of the Office's x coordinate.
     * @return x coordinate of the Office. 0 is the left-most Office.
     */
    public int getX() {
        return (o-1)%N;
    }

    /**
     * Getter of the Office's y coordinate.
     * @return y coordinate of the Office. 0 is the top-most Office
     */
    public int getY() {
        return (o-1)/N;
    }

    /**
     * Computes Manhattan distance to reach another office.
     * @param office Another office
     * @return Manhattan distance from this office to another office.
     */
    public int dist(Office office) {
        return Math.abs(getX()- office.getX()) + Math.abs(getY()- office.getY());
    }

    /**
     * Compares taking into account the office number (and of course, it
     * goes without saying that equality can occur if the other Object
     * is also an Office).
     * @param o Another object
     * @return True whether the two objects are equal (both Offices with the same number).
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Office office = (Office) o;

        return getOfficeNumber() == office.getOfficeNumber();
    }

    /**
     * Returns the office number as a hashCode.
     * @return Office number as hashCode
     */
    @Override
    public int hashCode() {
        return o;
    }

    /**
     * Returns String representation of the Office.
     * @return String representation of the Office.
     */
    @Override
    public String toString() {
        return "o" + getOfficeNumber();
    }
}
