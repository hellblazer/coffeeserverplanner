package com.hellblazer.misc;

/**
 * Custom exception that is used in our planenr.
 *
 * Created by sprkrd on 10/16/16.
 */
public class CoffeeServerPlannerException extends Exception {

    /**
     * Default constructor. No particularities.
     * @see Exception
     */
    public CoffeeServerPlannerException() {}

    /**
     * Constructor that takes a custom message
     * @param msg Exception's message. Same role than in superclass
     * @see Exception
     */
    public CoffeeServerPlannerException(String msg) {
        super(msg);
    }

    /**
     * Constructor that takes a custom message and the cause
     * for this Exception as a Throwable.
     * @param msg Same role than in superclass
     * @param cause Same role than in superclass
     * @see Exception
     */
    public CoffeeServerPlannerException(String msg, Throwable cause) {
        super(msg,cause);
    }
}
