package com.hellblazer.misc;

/**
 * Argument class. Operators and predicates may have a number of arguments.
 * This class provides very basic functionality to support shared arguments
 * (e.g. multiple references to the same argument) and
 * non-instantiated or unbound arguments (arguments without any value).
 *
 * Created by sprkrd on 10/16/16.
 */
public class Argument<T> implements Cloneable {

    private static int nextId = 0;
    private static int getNextId() {
        int id;
        // synchronized (Argument.class) { // Uncomment in case of multithreaded application
        id = nextId++;
        // }
        return id;
    }

    private int id;
    private T value;

    /**
     * Constructor. Creates argument with unique id.
     */
    public Argument() {
        this.id = getNextId();
    }

    /**
     * Constructor. Argument with initial value.
     *
     * @param value Initial value of the argument. value should be immutable.
     */
    public Argument(T value) {
        this.id = getNextId();
        this.value = value;
    }

    /**
     * Getter. For debugging it is more convenient to use consecutive integer ids than memory references.
     * Arguments can be shared among different predicates/operators in the stack. In such case, they shall
     * have the same object reference and, therefore, the same id.
     * @return Id of the argument
     */
    public int getId() {
        return id;
    }

    /**
     * Getter
     * @return Value of the argument (can be null).
     */
    public T getValue() {
        return value;
    }

    /**
     * Setter
     * @param value New value. It should be immutable to avoid weird bugs.
     *              Set to null to unbind this argument.
     */
    public void setValue(T value) {
        this.value = value;
    }

    /**
     * Convenience method. Makes sure that value is not null
     * @return true whether the argument is instantiated. False otherwise.
     */
    public boolean isBound() {
        return value != null;
    }

    /**
     * Checks if the values of two arguments are bound and are equal.
     *
     * @param argument Another Argument
     * @return true whether the Arguments values are equal, false otherwise
     */
    public boolean boundAndEqual(Argument<?> argument) {
        return equals(argument) && isBound();
    }

    /**
     * Clones Argument *without* replicating id.
     *
     * @return cloned Argument
     */
    @SuppressWarnings("CloneDoesntCallSuperClone")
    @Override
    public Object clone() {
        return new Argument<>(getValue());
    }

    /**
     * Overrides method so that the values of the arguments are compared.
     * If the other object is not an Argument this method cannot possibly return true.
     * @param o Another object. Must be an Argument in order for this method to return true.
     * @return True if the other object is an Argument and has the same value.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Argument<?> argument = (Argument<?>) o;

        return value!=null? value.equals(argument.value) : argument.value==null;

    }

    /**
     * Overrides hashCode so it returns the hashCode of the Argument's value.
     * @return hashCode of the Argument's value.
     */
    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    /**
     * @return String representation of the argument. If the argument is
     * unbound, the returned string follows the convention ?X[id]
     */
    @Override
    public String toString() {
        if (isBound()) {
            return value.toString();
        }
        else {
            return String.format("?X%d",getId());
        }
    }

}
