package com.hellblazer.operations;


import com.hellblazer.misc.Argument;
import com.hellblazer.predicates.*;
import com.hellblazer.state.State;

/**
 * Class that represents an Operator with two Arguments.
 *
 * For more info, we refer the reader to all the classes that extend this
 * abstract class.
 *
 * Created by sprkrd on 10/19/16.
 *
 * @see MakeCoffee
 * @see MoveRobot
 * @see ServeCoffee
 */
public abstract class BinaryOperator<T,U> implements Operation {
    final Argument<T> fst = new Argument<>();
    final Argument<U> snd = new Argument<>();

    PredicateList preconditions;
    PredicateList deleteList;
    PredicateList addList;

    /**
     * Getter
     * @return Preconditions list of this Operator (i.e. predicates that a state must
     * contain in order for this operator to be applied).
     */
    @Override
    public PredicateList getPreconditions() {
        return preconditions;
    }

    /**
     * Getter
     * @return Add list of this Operator (i.e. predicates that will be added to some
     * state after this Operator has been applied).
     */
    @Override
    public PredicateList getAddList() {
        return addList;
    }

    /**
     * Getter
     * @return Delete list of this Operator (i.e. predicates that will be deleted
     * from some state after this Operator has been applied).
     */
    @Override
    public PredicateList getDeleteList() {
        return deleteList;
    }

    /**
     * Tells whether all the arguments of this Operator are bound.
     * @return true if all the arguments of this Operator are bound.
     */
    @Override
    public boolean isFullyInstantiated() {
        return fst.isBound() && snd.isBound();
    }

    /**
     * Tells if the operator can be applied to a given state.
     * @param state A state (i.e. collection of fully instantiated predicates)
     * @return true if it is possible to apply this Operator.
     */
    @Override
    public boolean canApply(State state) {
        /* Make sure that operator is fully instantiated */
        if (!isFullyInstantiated()) {
            return false;
        }
        /* Check preconditions */
        for (Predicate predicate : preconditions) {
            if (!state.contains(predicate)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Apply this operator to a given state.
     * @param state State to which this operator will be applied (in-place).
     * @return The same state.
     */
    @Override
    public State apply(State state) {
        if (!canApply(state)) {
            return null;
        }
        /* Delete predicates from deleteList. */
        for (Predicate predicate : deleteList) {
            state.remove(predicate);
        }
        /* Add predicates from addList */
        for (Predicate predicate : addList) {
            state.add(predicate);
        }

        return state;
    }

    /**
     * Getter
     * @return First argument of the Operator.
     */
    public Argument<T> getFstArgument() {
        return fst;
    }

    /**
     * Getter
     * @return Second argument of the Operator.
     */
    public Argument<U> getSndArgument() {
        return snd;
    }

    /**
     * @return String representation of the operator.
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + '(' +
                fst + ',' + snd +
//                Uncomment the following lines in order to get
//                a more verbose output:
//                "|Pre:" + preconditions +
//                "|Add:" + addList +
//                "|Del:" + deleteList +
                ')';
    }
}
