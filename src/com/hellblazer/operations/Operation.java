package com.hellblazer.operations;

import com.hellblazer.predicates.PredicateList;
import com.hellblazer.stack.Stackable;
import com.hellblazer.state.State;

/**
 * Operation interface. Tries to define the common functionality of all
 * the Operations. This is, all the Operations have in common that they
 * have a list of Preconditions, an Add list and a Delete list. Therefore
 * all the Classes that implement this interface must implement methods
 * that returns these three lists. Moreover, an Operation can be applied
 * to a State, so the interface defines methods to check whether the
 * Operation is applicable and to effectively applying it. Last but not
 * least, an Operation may have parameters, so the interface defines a
 * method to tell whether the Operation is fully instantiated.
 *
 * For more info, we refer the reader to all the classes that implement this
 * interface.
 *
 * Created by salva on 05/10/2016.
 *
 * @see BinaryOperator
 * @see MakeCoffee
 * @see MoveRobot
 * @see ServeCoffee
 */
public interface Operation extends Stackable {
    /**
     * Gets the precondition list
     * @return Predicate array containing the preconditions
     *
     * @see PredicateList
     */
    PredicateList getPreconditions();

    /**
     * Returns the postcondition list
     * @return Predicate array containing the add list
     *
     * @see PredicateList
     */
    PredicateList getAddList();

    /**
     * Gets the delete list (not actually used in the current
     * implementation, but still nice to have).
     *
     * @return Predicate array containing the delete list
     *
     * @see PredicateList
     */
    PredicateList getDeleteList();

    /**
     * @return Whether all the Operation's Arguments are bound or not
     *
     * @see com.hellblazer.misc.Argument
     */
    boolean isFullyInstantiated();

    /**
     * Applies the Operation to a given state (in-place), if possible.
     *
     * @param state The method will infer if the Operation can be applied
     *              to this State.
     * @return true if the Operation can be applied.
     */
    boolean canApply(State state);

    /**
     * Applies the operation to a given state (in-place).
     * @param state State to which the Operation shall be applied.
     * @return The same state if the Operation has been applied
     * successfully. null otherwise.
     *
     * @see State
     */
    State apply(State state);

//    public static boolean producesPredicate(Predicate predicate) {
//        // By default, it does not produce anything
//        return false;
//    }
}
