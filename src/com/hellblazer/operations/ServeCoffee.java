package com.hellblazer.operations;

import com.hellblazer.misc.Office;
import com.hellblazer.predicates.*;

/**
 * ServeCoffee Operation. This Operation takes as argument both
 * an Office (where one or more coffee cups should be served)
 * and the number of cups to be served. As preconditions, it is
 * necessary that the robot is located at the Office where the
 * coffee should be served, that there is an actual petition at
 * that office and that the robot is loaded with the requested
 * number of coffee cups. After the Operation is applied, the
 * Petition is served (new Served predicate) and the robot is
 * free again to grab new cups.
 *
 * Created by sprkrd on 10/19/16.
 */
public class ServeCoffee extends BinaryOperator<Office,Integer> {

    /**
     * Constructor. Creates a non-bound Operator. The preconditions,
     * add list and delete list are set according to the description
     * of the predicate (see class description).
     *
     * @see ServeCoffee
     */
    public ServeCoffee() {
        preconditions = new SimplePredicateList(
                new Petition(fst,snd),
                new RobotLoaded(snd),
                new RobotLocation(fst));
//                new RobotLocation(fst),
//                new RobotLoaded(snd),
//                new Petition(fst,snd));

        deleteList = new SimplePredicateList(
                new Petition(fst,snd),
                new RobotLoaded(snd));
        addList = new SimplePredicateList(
                new Served(fst),
                new RobotFree());
    }

    /**
     * Tells if this operator produces a given Predicate (i.e. it is in its
     * Add list).
     *
     * @param predicate The method returns whether the operator produces this
     *                  predicate
     * @return true if predicate is of type Served or of type RobotFree
     */
    public static boolean producesPredicate(Predicate predicate) {
        return predicate instanceof Served || predicate instanceof RobotFree;
    }

}
