package com.hellblazer.operations;

import com.hellblazer.misc.Office;
import com.hellblazer.predicates.*;

/**
 * MakeCoffee predicate. It has two arguments: the Office where the
 * coffe should be prepared and the number of Coffee cups. In order
 * for this operator to be applicable it is required that the robot
 * is located at the office of the first argument, that the robot
 * is free and that there is a Machine with capacity to prepare
 * the number of coffees of the second argument. After this operator
 * is applied, the robot is loaded with the given number of cups of
 * coffees (i.e. no longer free).
 *
 * Created by sprkrd on 10/19/16.
 */
public class MakeCoffee extends BinaryOperator<Office,Integer> {

    /**
     * Constructor. Creates a non-bound Operator. The preconditions,
     * add list and delete list are set according to the description
     * of the predicate (see class description).
     *
     * @see MakeCoffee
     */
    public MakeCoffee() {
        preconditions = new SimplePredicateList(
                new Machine(fst,snd),
                new RobotLocation(fst),
                new RobotFree());
        deleteList = new SimplePredicateList(
                new RobotFree());
        addList = new SimplePredicateList(
                new RobotLoaded(snd));
    }

    /**
     * Tells if a given predicated is produced by this operator.
     *
     * @param predicate The predicate we want to test in order to see
     *                  if the operator is able to generate it.
     * @return true if predicate is a RobotLoaded predicate (the only
     * predicate this operator is able to produce).
     */
    public static boolean producesPredicate(Predicate predicate) {
        return predicate instanceof RobotLoaded;
    }
}
