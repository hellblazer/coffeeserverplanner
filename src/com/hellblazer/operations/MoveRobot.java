package com.hellblazer.operations;

import com.hellblazer.misc.Argument;
import com.hellblazer.misc.Office;
import com.hellblazer.predicates.*;
import com.hellblazer.state.State;

/**
 * MoveRobot operator. It has two Offices as arguments. The operator is
 * applicable whenever the robot is located at the Office of the first
 * argument. After appliying the operator, the robot will be at the Office
 * of the second argument. The number of steps is updated according to the
 * Manhattan distance between the two offices.
 *
 * Created by sprkrd on 10/19/16.
 */
public class MoveRobot extends BinaryOperator<Office,Office> {

    /**
     * Dummy argument to obtain the number of steps in the current state when binding.
     */
    private Argument<Integer> steps = new Argument<>();

    /**
     * Constructor. Creates a non-bound Operator. The preconditions,
     * add list and delete list are set according to the description
     * of the predicate (see class description).
     *
     * @see MoveRobot
     */
    public MoveRobot() {
        preconditions = new SimplePredicateList(
                new RobotLocation(fst),
                new Steps(steps));
        deleteList = new SimplePredicateList(
                new RobotLocation(fst),
                new Steps(steps));
        addList = new SimplePredicateList(
                new RobotLocation(snd));
    }

    /**
     * Small modification in the apply method to add the updated number of steps
     * @param state State to which this operator should be applied.
     * @return Updated state.
     */
    @Override
    public State apply(State state) {
        for (Predicate predicate : state) {
            if (predicate instanceof Steps) {
                steps.setValue(((Steps)predicate).getFstArgument().getValue());
                break;
            }
        }
        State ret = super.apply(state);
        int dist = fst.getValue().dist(snd.getValue());
        ret.add(new Steps(steps.getValue()+dist));
        return ret;
    }

    /**
     * Tells if this operator produces a given Predicate (i.e. it is in its
     * Add list).
     *
     * @param predicate The method returns whether the operator produces this
     *                  predicate
     * @return true if predicate is of type RobotLocation
     */
    public static boolean producesPredicate(Predicate predicate) {
        return predicate instanceof RobotLocation;
    }

}
