package com.hellblazer.stack;

/**
 * Interface that identifies all the Objects that can be stacked, namely
 * the Operations, the Predicates and the PredicateLists
 *
 * Created by salva on 05/10/2016.
 *
 * @see com.hellblazer.operations.Operation
 * @see com.hellblazer.predicates.Predicate
 * @see com.hellblazer.predicates.PredicateList
 */
public interface Stackable {
}
