package com.hellblazer.stack;

import com.hellblazer.heuristics.Heuristic;
import com.hellblazer.misc.CoffeeServerPlannerException;
import com.hellblazer.operations.MakeCoffee;
import com.hellblazer.operations.MoveRobot;
import com.hellblazer.operations.Operation;
import com.hellblazer.operations.ServeCoffee;
import com.hellblazer.predicates.Predicate;
import com.hellblazer.predicates.PredicateList;
import com.hellblazer.predicates.SimplePredicateList;
import com.hellblazer.predicates.SortedPredicateList;
import com.hellblazer.state.State;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Class responsible of managing the STRIPS stack. A heuristic class can be provided in order to enhance the
 * way the algorithm performs.
 * Created by burst on 23/10/2016.
 */
public class PlannerStack {

    private Stack<Stackable> stack = new Stack<>();
    private LinkedList<Operation> performed = new LinkedList<>();
    private State state;
    private Heuristic heuristic;

    /**
     * Creates a new STRIPS PlannerStack Object with the specified initial and final states
     * @param initialState Initial state from where the algorithm will proceed
     * @param goalState Goal state that should be reached
     */
    public PlannerStack(State initialState, State goalState) {
        this(initialState, goalState, new Heuristic());
    }

    /**
     * Creates a new STRIPS PlannerStack Object with the specified initial and final states
     * @param initialState Initial state from where the algorithm will proceed
     * @param heuristic Heuristic that will be used
     * @param goalState Goal state that should be reached
     */
    public PlannerStack(State initialState, State goalState, Heuristic heuristic) {
        this.state = initialState;
        if (heuristic == null) {
            // Defaults to the basic heuristic
            heuristic= new Heuristic();
        }
        this.heuristic = heuristic;
        this.pushGoal(goalState);
    }

    /**
     * Find the most suitable operation to be applied for the target predicate
     * @param predicate Target predicate that should be ensured
     * @return  Applicable Operation
     * @throws CoffeeServerPlannerException If no applicable operation is found
     */
    private Operation getValidOperation(Predicate predicate) throws CoffeeServerPlannerException {
        Operation result;
        if (MakeCoffee.producesPredicate(predicate)) {
            result = new MakeCoffee();
        } else if (MoveRobot.producesPredicate(predicate)) {
            result = new MoveRobot();
        } else if (ServeCoffee.producesPredicate(predicate)) {
            result = new ServeCoffee();
        } else {
            // There is no valid operation for this
            throw new CoffeeServerPlannerException("Predicate cannot be produced: "+predicate.toString());
        }

        // We now bind the predicates with the target
        PredicateList adds = result.getAddList();
        for (Predicate output: adds) {
            if (output.canBind(predicate)) {
                output.bind(predicate);
                break;
            }
        }

        return result;
    }

    /**
     * Returns the current state
     * @return Current state
     */
    public State getCurrentState() {
        return this.state;
    }

    /**
     * Returns a sorted list with all operations applied to date
     * @return Performed operation to reach the current state
     */
    public Collection<Operation> getPerformedOperations() {
        return this.performed;
    }

    /**
     * Adds a complex goal to the stack. Goals are predicate lists and can either represent a final state or the
     * activation predicates of an Operation
     * @param goalState Partial goal to be added.
     */
    private void pushGoal(PredicateList goalState) {
        // We add the list of goal predicates so that it can be checked afterwards
        this.stack.push(goalState);
    }

    /**
     * Adds an operation to the stack. Pushes both the actual operation and the goal representing the activation
     * predicates of the Operation.
     * @param op Operation to be pushed
     */
    private void pushOperation(Operation op) {
        // We add the list of goal predicates so that it can be checked afterwards
        this.stack.push(op);
        this.pushGoal(this.heuristic.getOperationGoal(this.state, op));
    }

    /**
     * Informs whether or not the stack has reached the target final state.
     * @return The stack has already archived the objective
     */
    public boolean everythingDone() {
        return this.stack.isEmpty();
    }

    /**
     * Removes the top element from the STRIPS stack and executes the corresponding actions.
     * @throws CoffeeServerPlannerException Throws exception if the top element of the stack could not be processed
     */
    public void processTop() throws CoffeeServerPlannerException {
        Stackable top = this.stack.pop();

        if (top instanceof Predicate) {
            // We should check if the predicate is checked and add a valid operation otherwise
            Predicate correspondence = this.heuristic.findCorrespondence(this.state, (Predicate)top);
            if (correspondence != null) {
                // It checks the target correspondence, no problems there
                ((Predicate) top).bind(correspondence);
            } else {
                // There is no correspondence found, we have to apply an operation so that we can check this predicate
                Operation op = this.getValidOperation((Predicate)top);
                // We return the predicate to the stack
                this.stack.push(top);
                this.pushOperation(op);
            }
        } else if (top instanceof Operation) {
            // We should execute the operation and modify the current state
            // We don't need to check if the state can be applied because the preconditions has been already checked
            ((Operation)top).apply(this.state);
            this.performed.add((Operation)top);
        } else if (top instanceof PredicateList) {
            SimplePredicateList unchecked = new SimplePredicateList();
            // We should check all predicates and add the unchecked ones to the stack
            for (Predicate predicate: ((PredicateList)top)) {
                if (!this.state.contains(predicate)) {
                    unchecked.push(predicate);
                }
            }

            if (!unchecked.isEmpty()) {
                // Not all conditions were checked, we should add each one of them to the stack
                // We should also add the PredicateList
                this.stack.push(top);
                SortedPredicateList toAdd = this.heuristic.getGoal(this.state, unchecked);
                this.stack.addAll(toAdd);
            }
        }
    }

    /**
     * Execute the STRIPS algorithm to reach the target final state. Returns whether or not that state could be reached.
     * @return True if the target state could be reached. False otherwise.
     */
    public boolean processStack() {
        try {

            while (!this.everythingDone()) {
                this.processTop();
                // System.out.println(this.toString());
            }

            // The target final state could be reached
            return true;

        } catch (CoffeeServerPlannerException ex) {
            // If there is any exception, the target final state could not be reached
            System.err.println(ex.toString());
            return false;
        }
    }

    /**
     * Prints the stack as String
     * @return String representing the stack
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("STRIPS Stack:\n\n");

        if (this.stack.isEmpty()) {
            sb.append("EMPTY STACK\n");
        } else {
            sb.append("----------------------------------------\n");
            for (Stackable stackable : this.stack) {
                sb.append(stackable.toString());
                sb.append("\n----------------------------------------\n");
            }
        }


        return sb.toString();
    }
}
